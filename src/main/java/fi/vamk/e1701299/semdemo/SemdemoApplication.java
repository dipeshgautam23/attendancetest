package fi.vamk.e1701299.semdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SemdemoApplication {
	@Autowired
	private AttendanceRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(SemdemoApplication.class, args);
	}

	@Bean
	public void initDate() {
		Attendance att1 = new Attendance("QwertyTest");
		Attendance att2 = new Attendance("PQR");
		Attendance att3 = new Attendance("SUV");
		repository.save(att1);
		repository.save(att2);
		repository.save(att3);

	}

}
